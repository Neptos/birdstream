(function () {
    'use strict';

    angular
        .module('birdStream.home')
        .config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when('/', {
                templateUrl: 'home/home.html',
                controller: 'HomeController',
                controllerAs: 'vm'
            });
        }])
})();