(function () {
    'use strict';

    require('../core/core.module');
    
    angular.module('birdStream.home', []);

    require('./home.controller');
    require('./home.routes');

})();