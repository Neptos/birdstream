(function () {
    'use strict';

    angular
        .module('birdStream.home')
        .controller('HomeController', HomeController);

    HomeController.inject = [];
    function HomeController() {
        var vm = this;

        vm.players = [];
        vm.files = [];
        vm.playerCount = 4;
        vm.playerWidth = 0;
        vm.playerHeight = 0;

        vm.startPlayer = startPlayer;
        vm.stopPlayer = stopPlayer;

        activate();

        ////////////////

        function activate() {
            vm.playerWidth = window.innerWidth/2 - 20;
            vm.playerHeight = vm.playerWidth*9/16;
            vm.files = [
                'hyljes',
                'hirv',
                'merikotkas',
                'toidumaja',
                'kasmu',
                'virukatus'
            ]
        }

        function startPlayer(id, file) {
            console.log(id);
            console.log(file);
            vm.players += jwplayer('player' + id).setup({
                'flashplayer': 'http://pontu.eenet.ee/player/player.swf',
                'streamer': 'rtmp://193.40.133.138/live',
                'file': file,
                'autostart': 'true',
                'stretch': 'uniform',
                'controlbar': 'over',
                'rtmp.tunneling': 'false',
                'width': vm.playerWidth,
                'height': vm.playerHeight,
                'audio': 'false'
            });
        }

        function stopPlayer(id) {

        }
    }
})();