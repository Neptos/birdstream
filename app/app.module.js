(function () {
  'use strict';
  
  require('./home/home.module');

  // Declare app level module which depends on views, and components
  angular.module('birdStream', [
    'ngRoute',
    'ngMaterial',
    'birdStream.core'
  ]);

})();