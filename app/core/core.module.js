(function () {
    'use strict';

    angular.module('birdStream.core', [
        'birdStream.home'
    ]);

    require('./filters/range.filter');

})();