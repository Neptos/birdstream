# Birdstream angular app

## Production requirements:
Hopefully none.

## Dev requirements:
- `npm`
- `browserify`
- `uglifyjs`

## Create bundle:
`browserify app/app.module.js | uglifyjs > app/bundle.js`

## Start dev server:
`npm start`
